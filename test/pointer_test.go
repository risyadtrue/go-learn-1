package test

import (
	"fmt"
	"testing"
)

type Location struct {
	City, Province string
}

func TestPointer(t *testing.T) {
	//var address1 *Location = &Location{"Bogor", "Jawa Barat"}
	location1 := Location{"Bogor", "Jawa Barat"}
	// initialize location2 ke memory address var location1 (same memory address)
	location2 := &location1
	// copy location1 to address3 (in the difference memory address)
	location3 := location1

	memLocation1 := fmt.Sprintf("%p", &location1)
	fmt.Println("memory address location1: ", memLocation1)
	fmt.Println()

	// location2
	// print address memory
	memLocation2 := fmt.Sprintf("%p", location2)
	fmt.Println("memory address location2: ", memLocation2)
	// print value in address memory
	fmt.Println("address value location2: ", *location2)
	fmt.Println()

	// location3
	memLocation3 := fmt.Sprintf("%p", &location3)
	fmt.Println("memory address location3: ", memLocation3)
	// print without pointer
	fmt.Println("value location3: ", location3)
	// change address 3
	location3.City = "Salatiga"
	location3.Province = "Jawa Tengah"
	fmt.Println("change location3: ", location3)
	// cek location1, is that changed?
	fmt.Println("location1 after change by location3: ", location1)
	fmt.Println()

	// change location1 from location2 (same address)
	location2.City = "Purwakarta"
	fmt.Println("location1 after change by location2: ", location1)

}

type Player struct {
	Name string
}

func (player *Player) GiveHimMVP() {
	// will get player struct in same address
	memPlayer1 := fmt.Sprintf("%p", player)
	fmt.Println("memory address inside method: ", memPlayer1)
	player.Name = player.Name + " (MVP)"
}

func (player Player) GiveHimMVPValue() {
	// in this function new player struct will be created
	// not change outside player struct
	player.Name = player.Name + " (MVP)"
	memPlayer2 := fmt.Sprintf("%p", &player)
	fmt.Println("memory address inside method: ", memPlayer2)
	fmt.Println("name in method: ", player.Name)
}

func TestPointerMethod(t *testing.T) {
	player1 := Player{
		Name: "Xepher",
	}

	fmt.Println("player1 name: ", player1.Name)
	memPlayer1 := fmt.Sprintf("%p", &player1)
	fmt.Println("memory address player1: ", memPlayer1)
	player1.GiveHimMVP()
	fmt.Println("name after pointer method: ", player1.Name)
	fmt.Println()

	player2 := Player{
		Name: "InYourDream",
	}

	fmt.Println("player2 name: ", player2.Name)
	memPlayer2 := fmt.Sprintf("%p", &player2)
	fmt.Println("memory address player2: ", memPlayer2)
	player2.GiveHimMVPValue()
	fmt.Println("name after method: ", player2.Name)
}
