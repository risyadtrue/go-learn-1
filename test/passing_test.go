package test

import (
	"fmt"
	"testing"
)

type User struct {
	Name string
}

func AddDegreeToUserValueReturn(user User) User {
	user.Name = user.Name + " S. Sc., M. Kom"
	return user
}

func AddDegreeToUserValue(user User) {
	user.Name = user.Name + " S. Sc., M. Kom"
	fmt.Println("print in function : ", user)
}

func AddDegreeToUserReference(user *User) {
	// passing 'user' by references
	user.Name = user.Name + " S. Sc., M. Kom"
}

func TestPassingByValue(t *testing.T) {
	fmt.Println("change user name by value")
	user := User{
		Name: "Ripang",
	}
	AddDegreeToUserValue(user)
	fmt.Println("change user name by value : ", user)
	fmt.Println()

	fmt.Println("change user name by value and return ")
	userReturn := User{
		Name: "Ripang",
	}
	userNew := AddDegreeToUserValueReturn(userReturn)
	fmt.Println(userNew)
	fmt.Println()

	fmt.Println("change user name by references")
	userRef := User{
		Name: "Ripang",
	}
	// passing address to function "&"
	AddDegreeToUserReference(&userRef)
	fmt.Println(userRef)
}
