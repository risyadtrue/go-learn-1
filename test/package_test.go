package test

import (
	"fmt"
	"go-learn-1/faktorial"
	"testing"
)

func TestFactorial(t *testing.T) {
	result, recursiveResult := faktorial.Factorial(5)
	fmt.Println("Factorial result without recursive", result)
	fmt.Println("Factorial result with recursive", recursiveResult)
}
