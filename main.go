package main

import "fmt"

func main() {
	// cek
	fmt.Println("this commit 1")
	// call private function
	res := factorialRecursive(3)
	fmt.Println(res)
}

// private function in main package
func factorialRecursive(number int) int {
	if number < 1 {
		return 1
	}

	return number * factorialRecursive(number-1)
}
