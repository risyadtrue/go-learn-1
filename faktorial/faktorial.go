package faktorial

func Factorial(number int) (int, int) {
	final := 1
	for i := number; i > 0; i-- {
		final *= i
	}

	// call private function in factorial package
	recursiveRes := factorialRecursive(number)

	return final, recursiveRes
}

// private function
func factorialRecursive(number int) int {
	if number < 1 {
		return 1
	}

	return number * factorialRecursive(number-1)
}
